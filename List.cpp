#include "List.h"

int List::GetLastValue() {
    if (head == nullptr) {
        return -1;
    }

    Node* current = head;
    while (current->next != nullptr) {
        current = current->next;
    }
    return current->value;
}

bool List::EraseElementByValue(int valueToFind) {
    if (head == nullptr)
    {
        return false;
    }

    if (head->value == valueToFind)
    {
        Node* temp = head;
        head = head->next;
        delete temp;
        return true;
    }

    Node* current = head;
    while (current->next != nullptr) {
        if (current->next->value == valueToFind) {
            Node* temp = current->next;
            current->next = current->next->next;
            delete temp;
            return true;
        }
        current = current->next;
    }
    return false;
}

bool List::EraseElementByPointer(Node* pointer) {
    if (head == nullptr) {
        return false;
    }

    if (head == pointer) {
        Node* temp = head;
        head = head->next;
        delete temp;
        return true;
    }

    Node* current = head;
    while (current->next != nullptr) {
        if (current->next == pointer) {
            Node* temp = current->next;
            current->next = current->next->next;
            delete temp;
            return true;
        }
        current = current->next;
    }
    return false;
}

Node* List::GetNodeByValue(int targetValue) {
    Node* current = head;
    while (current != nullptr) {
        if (current->value == targetValue) {
            return current;
        }
        current = current->next;
    }
    return nullptr;
}