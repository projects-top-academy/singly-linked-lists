﻿/*

Сделать односвязный список
Дан код односвязного списка, нужно реализовать для него 4 метода (описания ниже)
В классе List
Нужно проверить что операции работают (печатать в консоль какую то информацию, например вызывать PrintValuesInList после удаления элемента)


*/

#include <iostream>
#include "Node.h"
#include "List.h"

int main()
{
    List mylist;
    mylist.PushBack(1);
    mylist.PushBack(3);
    mylist.PushBack(5);

    mylist.PrintValuesInList();

    cout << "Last value in the list: " << mylist.GetLastValue() << "\n";

    if (mylist.EraseElementByValue(3))
    {
        cout << "Element with value 3 was successfully erased.\n";
        mylist.PrintValuesInList();
    }
    else {
        cout << "Element with value 3 was not found.\n";
    }

    Node* nodeToFind = mylist.GetNodeByValue(5);
    if (nodeToFind != nullptr)
    {
        if (mylist.EraseElementByPointer(nodeToFind))
        {
            cout << "Element with value 5 was successfully erased.\n";
            mylist.PrintValuesInList();
        }
        else {
            cout << "Element with value 5 was not found.\n";
        }
    }
    else {
        cout << "Element with value 5 was not found.\n";
    }

    return 0;
}
