#pragma once
#include <iostream>
#include "Node.h"

using namespace std;



class List {
public:
    List() : head(nullptr) {}

    void PrintValuesInList() const {
        cout << endl << "Values in the list: ";

        Node* current = head;
        while (current != nullptr) {
            cout << current->value << " ";
            current = current->next;
        }

        cout << endl;
    }

    void PushBack(int value) {
        Node* newNode = new Node(value);
        if (head == nullptr) {
            head = newNode;
        }
        else {
            Node* current = head;
            while (current->next != nullptr) {
                current = current->next;
            }
            current->next = newNode;
        }
    }

    int GetLastValue();

    bool EraseElementByValue(int valueToFind);

    bool EraseElementByPointer(Node* pointer);

    Node* GetNodeByValue(int targetValue);

    void PushTop(int newValue);

private:
    Node* head;
};

