#pragma once
class Node {
public:
    int value;
    Node* next;

    Node(int val) : value(val), next(nullptr) {}
};
